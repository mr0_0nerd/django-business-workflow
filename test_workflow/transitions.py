__author__ = 'ayme'

from business_workflow.transitions import ActionTransitionBase
from business_workflow.models import WorkflowHistorical

class Impresora(ActionTransitionBase):
    def imprimir(self, **kwargs):
        self.get_params_from_kwargs(**kwargs)
        self.initial_state = self.instance.mw_state
        self.set_state()
        WorkflowHistorical.objects.create(content_type=self.instance.mw_content_type,
                                          content_id=self.instance.pk,
                                          user=self.user,
                                          initial_state=self.initial_state,
                                          transition=self.transition,
                                          comment=self.build_history_comment())
        print(self.instance, self.comment)
        return True
        #print(self.instance, self.comment)
