# coding=utf-8
from django.core.exceptions import ImproperlyConfigured
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models.signals import pre_save
from django.utils.translation import ugettext_lazy as _
from types import MethodType
from workflows.models import Workflow, WorkflowModelRelation, State
from .listeners import set_state
from .models import ModelWorkflowBase


def workflow_enabled(cls):
    if models.Model not in cls.__mro__:
        raise ImproperlyConfigured(_('The decorator "workflow_enabled" only applies to subclass of Django Model'))
    bases = list(cls.__bases__)
    bases.insert(0, ModelWorkflowBase)
    cls.__bases__ = tuple(bases)

    def get_workflow_id(self):
        content_type = ContentType.objects.get_for_model(cls)
        try:
            return WorkflowModelRelation.objects.get(content_type=content_type).workflow.id
        except WorkflowModelRelation.DoesNotExist:
            workflow = Workflow.objects.get_or_create(name='Empty Workflow')[0]
            WorkflowModelRelation.objects.create(content_type=content_type, workflow=workflow)
            return workflow.id

    current_state = models.ForeignKey(State, verbose_name=_(u"State"), name='current_state', null=True, blank=True)
    current_state.contribute_to_class(cls=cls, name='current_state')
    setattr(cls, 'get_workflow_id', MethodType(get_workflow_id, cls))
    pre_save.connect(set_state, sender=cls)
    return cls