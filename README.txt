==========================
Django Business Workflow
==========================

Business workflow for Django>=1.6.1

django-business-workflows provides a generic workflow engine for Django.
Any Django model can be a "workflow", only by adding a decorator to model definition.


Uses the implementation of django-workflows (by Kai Diefenbach).


Changelog
=========

0.1.0
-----

Added class to save the historic of workflow. 
Added a class that allow a custom Transition configuration from using external class.
Added business states for grouping workflow state.
Added decorator "workflow_enabled" for applies to subclass of Django Model.


Usage
-----

1. Install and add django-permission >= 1.0.3; django-workflows >= 1.0.2

 INSTALLED_APPS = (
        ... 
    'permission',
    'workflows'
)

2. Run ``python setup.py install`` to install.

3. Modify your Django settings to use ``business_workflow``:

4. Create states, workflow, transitions and start using the workflow.
